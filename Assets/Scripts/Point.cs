﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point: MonoBehaviour {

    void Start(){
        var tag = gameObject.tag;
        var nbPoint = GameObject.FindGameObjectsWithTag(tag);
        var nbTotal = 0;
        foreach (GameObject nb in nbPoint){
            nbTotal = nbTotal + 1;
        }

        if(tag == "point_bleu"){
            transform.position = new Vector3(nbTotal, 18.5f, 0);
        }else{
            transform.position = new Vector3(-nbTotal, 18.5f, 0);
        }

        if(nbTotal == 5){
            foreach (GameObject point in nbPoint){
                Destroy(point);
            }

            var autresPoints = new GameObject[0];
            if(tag == "point_bleu"){
                autresPoints = GameObject.FindGameObjectsWithTag("point_orange");
            }else{
                autresPoints = GameObject.FindGameObjectsWithTag("point_bleu");
            }
            foreach (GameObject point in autresPoints){
                Destroy(point);
            }
        }

    }

    void Update() {

    }
}
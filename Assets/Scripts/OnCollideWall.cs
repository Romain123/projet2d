using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnCollideWall : MonoBehaviour {
    public GameObject effect;

	private void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.layer == LayerMask.NameToLayer("Ball"))
        {
            var instance = Instantiate(effect, (Vector3)other.contacts[0].point, Quaternion.identity);
        }

	}
}
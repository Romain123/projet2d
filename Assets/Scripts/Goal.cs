﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal: MonoBehaviour {
   public GameObject Balle;
   public GameObject Point;
   public GameObject effect;
   public AudioClip goalSound;

   // Start is called before the first frame update 
   void Start() {
      GetComponent<AudioSource> ().playOnAwake = false;
      GetComponent<AudioSource> ().clip = goalSound;
   }

   // Update is called once per frame
   void Update() {

   }

   void OnCollisionEnter2D(Collision2D monObjet) {
      if (monObjet.gameObject.CompareTag("balle")) {
         Destroy(monObjet.gameObject);
         var point = Instantiate(Point);
         var instance = Instantiate(effect, (Vector3)monObjet.contacts[0].point, Quaternion.identity);
         GetComponent<AudioSource>().Play();

         StartCoroutine(waiter());
      }
   }

   IEnumerator waiter() {
      yield return new WaitForSeconds(3);
      var balle = Instantiate(Balle);

      var joueur1 = GameObject.FindGameObjectsWithTag("Joueur1");
      var joueur2 = GameObject.FindGameObjectsWithTag("Joueur2");
      joueur1[0].transform.position = new Vector3(-4, 0, 0);
      joueur2[0].transform.position = new Vector3(4, 0, 0);
   }
}
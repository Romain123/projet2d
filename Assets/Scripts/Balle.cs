﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balle: MonoBehaviour {

    //Mise a jour toute les 1 secondes
    // private float movementSpeed = 1f;
    // private float directionX;
    // private float directionY;
    // private int nextUpdate = 1;
    public GameObject effect;
    public AudioClip kickSound;

    void Start(){
        transform.position = new Vector3(0, 0, 0);
        GetComponent<AudioSource> ().playOnAwake = false;
        GetComponent<AudioSource> ().clip = kickSound;
    }
    void Update() {
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        var instance = Instantiate(effect, (Vector3)other.contacts[0].point, Quaternion.identity);
        GetComponent<AudioSource>().Play();
    }

}
﻿using UnityEngine;

 [RequireComponent(typeof(SpriteRenderer), typeof(Animator), typeof(Rigidbody2D))]
 public class AnimateGirlSpritesheet : MonoBehaviour {
     public KeyCode RightArrowKey = KeyCode.RightArrow;
     public KeyCode LeftArrowKey = KeyCode.LeftArrow;
     public KeyCode UpArrowKey = KeyCode.UpArrow;
     public KeyCode DownArrowKey = KeyCode.DownArrow;
    SpriteSheetAnimator animator;
    SpriteRenderer spriteRenderer;
    private float movementSpeed = 8f;



    void Start() {
        animator = GetComponent<SpriteSheetAnimator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate() {
        Vector3 move = Vector3.zero;
        if(Input.GetKey(RightArrowKey)|| Input.GetKey(LeftArrowKey) || Input.GetKey(UpArrowKey) || Input.GetKey(DownArrowKey)){
            if (Input.GetKey(RightArrowKey)) {
                move += Vector3.right * movementSpeed;
                spriteRenderer.flipX = false;
                animator.Play(Anims.Run);
            }
            if (Input.GetKey(LeftArrowKey)) {
                move += Vector3.left * movementSpeed;
                spriteRenderer.flipX = true;
                animator.Play(Anims.Run);
            }
            if (Input.GetKey(UpArrowKey)) {
                move += Vector3.up * movementSpeed;
                animator.Play(Anims.Climb);
            }
            if (Input.GetKey(DownArrowKey)) {
                move += Vector3.down * movementSpeed;
                animator.Play(Anims.Roll);
            }
        }else{
            animator.Play(Anims.Iddle);
        }
        GetComponent<Rigidbody2D>().velocity = move;
    }
}
